
class ParensNestingValidator(strToValidate: String) {

  private val specialChars =
    mapOf('(' to ')', '[' to ']', '<' to '>')

  private val specialCharsReverseLookup = specialChars
    .entries.associate { (k, v) -> v to k }

  private val allSpecialChars =
    specialChars.keys.union(specialChars.values).toSet()

  private val relevantChars =
    strToValidate.toList().intersect(allSpecialChars).toMutableList()

  private fun MutableList<Char>.removePairOfMatches(index: Int ){
    this.removeAt(index)
    this.removeAt(index)
  }

  fun validateParensNesting(): Boolean {
    if(relevantChars.isEmpty()) return true
    val firstEndingIndex = relevantChars.indexOfFirst { specialChars.values.contains(it) }
    if(firstEndingIndex == -1) return false
    val endingChar = relevantChars[firstEndingIndex]
    val relevantFirstChar = specialCharsReverseLookup[endingChar]
    val tempRelevantCharList = relevantChars.take(firstEndingIndex)
    val closestRelevantFirstIndex = tempRelevantCharList.indexOfLast { it == relevantFirstChar }
    val closestAnyFirstIndex = tempRelevantCharList.indexOfLast{ specialChars.keys.contains(it) }

    if (
      closestRelevantFirstIndex == -1 ||
      firstEndingIndex < closestRelevantFirstIndex ||
      closestAnyFirstIndex > closestRelevantFirstIndex
    ) return false

    relevantChars.removePairOfMatches(closestRelevantFirstIndex)

    validateParensNesting()//recurse

    //not reached
    return true
  }
}