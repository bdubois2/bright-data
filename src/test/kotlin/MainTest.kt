import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class MainTest{
  private var parensValidator: ParensNestingValidator? = null

  @BeforeEach
  fun initialize(){
    parensValidator = null
  }

  @Test
  fun `it validates that nothing still is valid`(){
    parensValidator = ParensNestingValidator("")

    val expected = parensValidator!!.validateParensNesting()

    assertTrue(expected)
  }

  @Test
  fun `it validates that a string with no chars passes`(){
    parensValidator = ParensNestingValidator("someStr")

    val expected = parensValidator!!.validateParensNesting()

    assertTrue(expected)
  }

  @Test
  fun `it validates that a string with any one parens or special chars is detected as false`(){

    parensValidator = ParensNestingValidator("(")
    var expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)

    parensValidator = ParensNestingValidator(")")
    expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)

    parensValidator = ParensNestingValidator("[")
    expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)

    parensValidator = ParensNestingValidator("]")
    expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)

    parensValidator = ParensNestingValidator("<")
    expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)

    parensValidator = ParensNestingValidator(">")
    expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)
  }

  @Test
  fun `it validates that a string with any open and closed parens is true`() {
    parensValidator = ParensNestingValidator("()")
    val expected = parensValidator!!.validateParensNesting()
    assertTrue(expected)
  }
  @Test
  fun `it validates that a string with any open and closed brackets is true`() {
    parensValidator = ParensNestingValidator("[]")
    val expected = parensValidator!!.validateParensNesting()
    assertTrue(expected)
  }

  @Test
  fun `it validates that a string with any open and closed square brackets is true`() {
    parensValidator = ParensNestingValidator("<>")
    val expected = parensValidator!!.validateParensNesting()
    assertTrue(expected)
  }

  @Test
  fun `it validates open and closed parens with other chars ignored`() {
    parensValidator = ParensNestingValidator("---(++++)----")
    val expected = parensValidator!!.validateParensNesting()
    assertTrue(expected)
  }
  @Test
  fun `it validates a more elaborate string with combination open and close parens`() {
    parensValidator = ParensNestingValidator("before ( middle []) after ")
    val expected = parensValidator!!.validateParensNesting()
    assertTrue(expected)
  }
  @Test
  fun `a small string with opposite parens is invalid`() {
    parensValidator = ParensNestingValidator(") (")
    val expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)
  }

  @Test
  fun `a string with non-consecutive nesting of parens and brackets is invalid`() {
    parensValidator = ParensNestingValidator("<(   >)")
    val expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)
  }

  @Test
  fun `it shows a string with an assortment of normal nesting is valid`() {
    parensValidator = ParensNestingValidator("fdafdsfds[  <>  ()  ]  <>  )")
    val expected = parensValidator!!.validateParensNesting()
    assertTrue(expected)
  }

  @Test
  fun `it shows a string with an combination of unbalanced nesting is invalid`() {
    parensValidator = ParensNestingValidator("(      [)")
    val expected = parensValidator!!.validateParensNesting()
    assertFalse(expected)
  }

}
